package ru.ibs.demo.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    @Bean
    public BasicDataSource dataSource() {
        String url = "jdbc:postgresql://localhost:5432/gods";
        BasicDataSource basic = new BasicDataSource();
        basic.setDriverClassName("org.postgresql.Driver");
        basic.setUrl(url);
        basic.setUsername("postgres");
        basic.setPassword("postgres");
        return basic;
    }

}
