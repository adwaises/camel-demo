package ru.ibs.demo.service;

public interface CamelService {
    void log() throws Exception;

    void copyFiles() throws Exception;

    void selectSqlToFile() throws Exception;

    void migrateTable() throws Exception;

    void fileToDb() throws Exception;

    void copyFileSimpleExample() throws Exception;

    void getRequest() throws Exception;

    void errorExample() throws Exception;

    void toFromKafka() throws Exception;
}
