package ru.ibs.demo.service;

import lombok.RequiredArgsConstructor;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaConstants;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.support.DefaultMessage;
import org.apache.camel.support.SimpleRegistry;
import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.stereotype.Service;

import java.util.concurrent.CountDownLatch;

import static org.apache.camel.component.http.HttpMethods.GET;

@Service
@RequiredArgsConstructor
public class CamelServiceImpl implements CamelService {

    private final BasicDataSource targetDataSource;

    @Override
    public void log() throws Exception {
        CamelContext context = new DefaultCamelContext();
        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("timer:foo")
                        .log("Hello Camel");
            }
        });
        context.start();
        Thread.sleep(3000);
        context.stop();
    }

    @Override
    public void copyFiles() throws Exception {
        CamelContext context = new DefaultCamelContext();

        context.getPropertiesComponent().setLocation("classpath:application.properties");
        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("file:{{from}}?noop=true")
                        .routeId("copy files")
//                        .log(">>>>>> ${body}")
                        .convertBodyTo(String.class)
                        .to("log:?showBody=true&showHeaders=true")
                        .choice()
                        .when(exchange -> String.valueOf(exchange.getIn().getBody()).endsWith(":a"))
                        .to("file:{{toA}}")
                        .when(exchange -> String.valueOf(exchange.getIn().getBody()).endsWith(":b"))
                        .to("file:{{toB}}")
                        .otherwise()
                        .to("file:{{toB}}");
            }
        });
        context.start();
        Thread.sleep(4000);
        context.stop();
    }

    @Override
    public void selectSqlToFile() throws Exception {

        SimpleRegistry reg = new SimpleRegistry();
        reg.bind("sourceDataSource", sourceDataSource());

        CamelContext context = new DefaultCamelContext(reg);
        context.getPropertiesComponent().setLocation("classpath:application.properties");

        context.addRoutes(new RouteBuilder() {
            @Override
            public void configure() {
                from("timer:base?period=60s")
                        .routeId("JDBC route")
                        .setHeader("key", constant(3))
                        .setBody(simple("select * from stg_risk.v_effects_361_dim where pk_id > :?key"))
                        .to("jdbc:sourceDataSource?useHeadersAsParameters=true")
                        .log(">>>> ${body}")
                        .process(exchange -> {
                            Message in = exchange.getIn();
                            Object body = in.getBody();
                            DefaultMessage message = new DefaultMessage(exchange);
                            message.setHeaders(in.getHeaders());
                            message.setHeader("rnd", "header");
                            message.setBody(body.toString() + System.lineSeparator() + in.getHeaders().toString());
                            exchange.setMessage(message);
                        })
                        .to("file:{{toB}}")
                        .toD("file:{{toB}}?fileName=done-${date:now:yyyyMMdd}-${headers.rnd}.txt");
            }
        });
        context.start();
        Thread.sleep(4000);
        context.stop();
    }

    @Override
    public void migrateTable() throws Exception {
        SimpleRegistry reg = new SimpleRegistry();
        reg.bind("sourceDataSource", sourceDataSource());
        reg.bind("targetDataSource", targetDataSource);

        CamelContext context = new DefaultCamelContext(reg);
        context.getPropertiesComponent().setLocation("classpath:application.properties");
        context.addRoutes(new RouteBuilder() {
            @Override
            public void configure() {
                from("direct:myOtherDirect")
                        .to("sql:select * from stg_risk.v_effects_361_dim?dataSource=#sourceDataSource")
                        .split(body())
                        .to("sql:insert into stg_risk.v_effects_361_dim_1 (pk_id, parent_id, level_num, elem_value, no_data_elem) " +
                                "values (:#pk_id, :#parent_id, :#level_num, :#elem_value, :#no_data_elem)?dataSource=#targetDataSource");
            }
        });
        context.start();
        ProducerTemplate template = context.createProducerTemplate();
        template.sendBody("direct:myOtherDirect", "");
        template.sendBody("file://{{toA}}?fileName=event-${date:now:yyyyMMdd-HH-mm}.html",
                "<hello>world</hello>");
        Thread.sleep(3000);
        context.stop();
    }

    @Override
    public void fileToDb() throws Exception {
        SimpleRegistry reg = new SimpleRegistry();
        reg.bind("targetDataSource", targetDataSource);

        CountDownLatch latch = new CountDownLatch(1);

        CamelContext context = new DefaultCamelContext(reg);
        context.getPropertiesComponent().setLocation("classpath:application.properties");
        context.addRoutes(new RouteBuilder() {
            @Override
            public void configure() {
                onCompletion()
                        .log(">>>>>>>>>>> Done")
                        .process(exchange -> latch.countDown());
                from("file:{{dataToDb}}?noop=true")
                        .unmarshal()
                        .json(true)
                        .log(">>>> ${body}")
                        .split(body())
                        .to("sql:insert into stg_risk.v_effects_361_dim_1 (pk_id, parent_id, level_num, elem_value, no_data_elem) " +
                                "values (:#pk_id, :#parent_id, :#level_num, :#elem_value, :#no_data_elem)?dataSource=#targetDataSource");
            }
        });
        context.start();
        //Thread.sleep(10000);
        latch.await();
        context.stop();
    }

    @Override
    public void copyFileSimpleExample() throws Exception {
        CamelContext context = new DefaultCamelContext();
        context.getPropertiesComponent().setLocation("classpath:application.properties");

        String a = "file:{{dataToDb}}?noop=true";
        String b = "file:{{toB}}";

        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from(a).to(b);
            }
        });
        context.start();
        Thread.sleep(4000);
        context.stop();
    }

    @Override
    public void getRequest() throws Exception {

        CamelContext context = new DefaultCamelContext();
        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("direct:rest")
                        .setHeader(Exchange.HTTP_METHOD, constant(GET))
                        .to("https://mail.ru")
                        .log("${body}");
            }
        });

        context.start();
        ProducerTemplate template = context.createProducerTemplate();
        template.sendBody("direct:rest", "");
        Thread.sleep(3000);
        context.stop();

    }

    @Override
    public void errorExample() throws Exception {
        SimpleRegistry reg = new SimpleRegistry();
        reg.bind("targetDataSource", targetDataSource);
        CamelContext context = new DefaultCamelContext(reg);
        context.getPropertiesComponent().setLocation("classpath:application.properties");
        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("file:{{dataToDb}}?noop=true")
                        .unmarshal()
                        .json(true)
                        .marshal()
                        .jacksonxml()
                        .log("${body}");
            }
        });
        context.start();
        Thread.sleep(1000);
        context.stop();
    }


    @Override
    public void toFromKafka() throws Exception {
        SimpleRegistry reg = new SimpleRegistry();
        reg.bind("targetDataSource", targetDataSource);
        CamelContext context = new DefaultCamelContext(reg);
        context.getPropertiesComponent().setLocation("classpath:application.properties");
        context.addRoutes(new RouteBuilder() {
            public void configure() {
                from("timer:kafka")
                        .setBody(constant("Message from Camel"))          // Message to send
                        .setHeader(KafkaConstants.KEY, constant("Camel")) // Key of the message
                        .to("kafka:{{kafka.topic}}?brokers={{kafka.broker}}");

            }
        });
        context.addRoutes(new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("kafka:{{kafka.topic}}?brokers={{kafka.broker}}")
                        .log("Message received from Kafka : ${body}")
                        .log("    on the topic ${headers[kafka.TOPIC]}")
                        .log("    on the partition ${headers[kafka.PARTITION]}")
                        .log("    with the offset ${headers[kafka.OFFSET]}")
                        .log("    with the key ${headers[kafka.KEY]}")
                        .to("sql:insert into test.test_table (key, value) values (:#${headers[kafka.KEY]}, :#${body})");
            }
        });
        context.start();
        Thread.sleep(10000);
        context.stop();
    }

    public BasicDataSource sourceDataSource() {
        String url = "jdbc:postgresql://localhost:5432/gods";
        BasicDataSource basic = new BasicDataSource();
        basic.setDriverClassName("org.postgresql.Driver");
        basic.setUrl(url);
        basic.setUsername("postgres");
        basic.setPassword("postgres");
        return basic;
    }

    public BasicDataSource targetDataSource() {
        String url = "jdbc:postgresql://localhost:5432/gods";
        BasicDataSource basic = new BasicDataSource();
        basic.setDriverClassName("org.postgresql.Driver");
        basic.setUrl(url);
        basic.setUsername("postgres");
        basic.setPassword("postgres");
        return basic;
    }

}
