package ru.ibs.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.ibs.demo.service.CamelService;

@SpringBootTest
class CamelDemoApplicationTests {

    @Autowired
    private CamelService camelService;

    @Test
    void contextLoads() {
    }

    @Test
    void log() throws Exception {
        camelService.log();
    }

    @Test
    void testCopyFiles() throws Exception {
        camelService.copyFiles();
    }

    @Test
    void testSelectSql() throws Exception {
        camelService.selectSqlToFile();
    }

    @Test
    void testMigrateTable() throws Exception {
        camelService.migrateTable();
    }

    @Test
    void testFileToDb() throws Exception {
        camelService.fileToDb();
    }

    @Test
    void testCopyFileSimpleExample() throws Exception {
        camelService.copyFileSimpleExample();
    }

    @Test
    void testGetRequest() throws Exception {
        camelService.getRequest();
    }

    @Test
    void testError() throws Exception {
        camelService.errorExample();
    }

    @Test
    void testKafka() throws Exception {
        camelService.toFromKafka();
    }

}
